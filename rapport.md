# Rapport de Métaheuristique

## Équipe

+ CALVILLO Eduardo
+ TRAORÉ Fayçal 

## Introduction

Nous allons utiliser tout au long des scéances de TP le langage Java parce que c'est celui dans lequel nous nous sentons le plus à l'aise.


## 3.1  Première étape (1 séance)
 1. *Ecrire un programme qui permet de lire les jeux de données*

	Nous avons écrit un parseur en Java (une fonction) qui lit les données depuis un fichier dont le nom est fourni en paramètre et rempli une structure de données **Data** que nous avons défini
	Le parseur applique en même temps un filtre sur les données lues de sorte à éliminer les noeud, arcs qui ne sont pas traversés par un chemin

	+ Lecture des fichiers

	```java
	public static void readFiles(String fileName, boolean isInput) {
		AtomicInteger cmp = new AtomicInteger();
		try {
			Files.lines(Paths.get("./" + fileName)).forEach(line -> {
				if (!line.trim().isEmpty()) {
					if (isInput)
						parseData(line, cmp);
					else {
						parseResult(line, cmp);
						cmp.incrementAndGet();
					}
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    ```

	+ Parseur du fichier lu ligne par lignes
	```java
	public static void parseData(String line, AtomicInteger cmp) {

		String[] split = line.split(" ");
		if (split[0].equals("c")) {
			cmp.incrementAndGet();

		} else if (cmp.intValue() == 1) {
			if (split.length == 2) {
				data.numEvacNodes = Integer.parseInt(split[0]);
				data.idOfSafeNode = Integer.parseInt(split[1]);
			} else {
				Vector<Integer> values = new Vector<>();
				values.add(int_(split[0]));
				data.addArc(new Arc(int_(split[0]), int_(split[4])));
				for (int i = 4; i < split.length - 1; i++) {
					values.add(int_(split[i]));
					data.addArc(new Arc(int_(split[i]), int_(split[i + 1])));
				}
				values.add(int_(split[split.length - 1]));
				values.add(-1);
				data.addArc(new Arc(int_(split[split.length - 1]), -1, Integer.MAX_VALUE, 0, Integer.MAX_VALUE));
				data.chemins.add(new Chemin(int_(split[0]), int_(split[1]), int_(split[2]), values));
			}

		} else {
			if (split.length == 2) {
				data.numNode = Integer.parseInt(split[0]);
				data.numEdge = Integer.parseInt(split[1]);
			} else {
				int tmp = 0;
				try {
					tmp = int_(split[2]);
				} catch (NumberFormatException e) {
					tmp = Integer.MAX_VALUE;
				}

				int index = data.arcs.indexOf(new Arc(int_(split[0]), int_(split[1])));
				if (index != -1) {
					data.arcs.get(index).set(tmp, int_(split[3]), int_(split[4]));
				}
			}
		}
	}
	```

## 3.1.2 Vérification et évaluation  d'une solution

 1. Créez un fichier représentant les données de l’exemple de la section 2.5 et un fichier représentantune solution pour cette instance (borne inférieure et/ou solution réalisable).

 	Ces fichiers ont comme nom au niveau de notre git `exemple.txt` et `real.solutionExemple.txt`

 2. Ecrivez une méthode permettant de vérifier si la solution fournie est réalisable et de vérifi er si la valeur de la fonction objectif est correcte.
   
	Nous avons pour cela procédé de la manière suivante:
    + Création d'un parseur permettant de lire le fichier solution
  
	```Java
	public static void parseResult(String line, AtomicInteger cmp) {
		if (cmp.get() == 0)
			result.fileName = line;
		else if (cmp.get() == 1)
			result.nbrSommets = int_(line);
		else if (cmp.get() > 1 && cmp.get() < result.nbrSommets + 2) {
			String[] split = line.split(" ");
			int tmpFlow = int_(split[1]);
			result.sommets.add(new Sommet(int_(split[0]), tmpFlow, int_(split[2])));
			Chemin ch = data.getChemin(int_(split[0]));
			ch.startDate = int_(split[2]);
			if (ch.flow < tmpFlow) {
				System.err.println("invalid: (flow overflow node [" + ch.id + "])");
				System.exit(-1);
			}
			ch.flow = tmpFlow;
		} else if (cmp.get() == result.nbrSommets + 2)
			result.isValid = (line.equals("valid")) ? true : false;
		else if (cmp.get() == result.nbrSommets + 3)
			result.fonctionObj = (line.equals("inf")) ? Integer.MAX_VALUE : int_(line);
		else if (cmp.get() == result.nbrSommets + 4)
			result.tempsCalcul = Double.parseDouble(line);
		else if (cmp.get() == result.nbrSommets + 5)
			result.methode = line;
		else
			result.commentaire = line;
	}
	```
    + Création d'un simulateur qui permet de simuler l'évacuation en se basant sur les données lues à partir du fichier solution

	Celui ci est basé sur un principe d'évènements et calcul les différents évènements qui ont lieu au niveau de chaque noeud traversé par un chemin

	```java
	public static void simulate(Data donnees) {
		for (Chemin ch : donnees.chemins) {
			int temps = ch.startDate;
			int blockSize = (int) Math.ceil(ch.size / (double) ch.flow);
			int remainingBlock = ch.size % ch.flow;
			temps = computeEvents(donnees, ch, temps, blockSize, remainingBlock);
			donnees.endDate = (donnees.endDate < temps + blockSize) ? temps + blockSize : donnees.endDate;
		}
	}
	```

	et voici la méthode `computeEvents` qui calcul les évènements

	```java
	private static int computeEvents(Data donnees, Chemin ch, int temps, int blockSize, int remainingBlock) {
        ch.tempsTraversee = 0;
        for (int i = 0; i < ch.path.size() - 1; i++) {
            int start = ch.path.get(i);
            int end = ch.path.get(i + 1);
            Arc arc = donnees.getArc(start, end);
            if (arc == null) {
                System.err.println("Arcs null (non trouvé)");
                System.exit(-1);
            }
            arc.addEvent(temps, ch.flow);
            if (remainingBlock == 0)
                arc.addEvent(temps + blockSize, -ch.flow);
            else {
                arc.addEvent(temps + blockSize - 1, remainingBlock - ch.flow);
                arc.addEvent(temps + blockSize, -remainingBlock);
            }
            if (i != ch.path.size() - 2) {
                temps += arc.time;
                ch.tempsTraversee += arc.time;
            }
        }
        ch.tempsTraversee += blockSize + remainingBlock;
        return temps;
    }
	```

    + Comparaison des résultats obtenus à partir de notre simulation (date de fin) avec ceux fournis par le fichier solution et vérification par le même fonction du fait que la solution soit réalisable ou pas



	```java
	public static int checker(Data donnees, int fonctionObj) {
        /**
         * 0 => invalid
         * 3 => valid for duedate
         * 4 => valid for capacity
         * 7 => valid
         */
        int res = 0;
        boolean isValid = true, isCapacityValid = true, isDueDateValid = true, isFonctionObjValid = true;
        isFonctionObjValid = fonctionObj == donnees.endDate;
        for (Chemin ch : donnees.chemins) {
            if (ch.flow > ch.realMaxFlow) {
                isCapacityValid = false;
            }
        }
        for (Arc arc : donnees.arcs) {
            int max = arc.capacity;
            int currentFlow = 0;
            int dueDate = arc.dueDate;
            for (Pair p : arc.events) {
                currentFlow += p.value;
                if (currentFlow > max)
                    isCapacityValid = false;
            }
            int realDueDate = arc.events.last().time + arc.time;
            if (realDueDate > dueDate) {
                dueDate + " vs " + realDueDate);
                isDueDateValid = false;
            }

        res = (isDueDateValid)? res + 3 : res;
        res = (isCapacityValid) ? res + 4 : res;
		if(DEBUG){
			System.out.print("is_valid: ");
			if (isCapacityValid && isDueDateValid && isFonctionObjValid) {
				System.out.println("true");

			} else if (isCapacityValid && !isDueDateValid && isFonctionObjValid || !isCapacityValid && isDueDateValid && isFonctionObjValid) {
				System.out.println("semi-valid");
			} else {
				System.out.println("false");
			}
			System.out.println("capacity_ok: " + isCapacityValid);
			System.out.println("deadline_ok: " + isDueDateValid);
			System.out.println("end_date: " + isFonctionObjValid);
			System.out.println("diff_to_best_valid: " + 0);
			System.out.println("diff_to_best_capacity_only: " + 0);
			System.out.println("diff_to_best_deadline_only: " + 0);
			System.out.println("\n\n\n\nEnd date = [" + donnees.endDate + "]");
		}
        return res;
    }
	```

	+ Résultat de l'application de notre méthode de vérification sur l’exemple.

	```java
	is_valid: true
	capacity_ok: true
	deadline_ok: true
	end_date: true
	diff_to_best_valid: 0
	diff_to_best_capacity_only: 0
	diff_to_best_deadline_only: 0

	End date = [37]
	Compute Time: 1ms.
	```
	+ Pouvez-vous donner la complexité de votre algorithme 
  
		L'algorithme de vérification a une complexité en O(n)

	+ Quel est l’intérêt de cette méthode devérification pour le problème d’optimisation étudié ?
		L'intérêt de cette méthode de vérification est qu'elle est "facile"à mettre ne place et réduit assez le nombre d'itérations car une méthodes de vérification qui aurait au sein d'elle même une horloge serait vrai onéreuse.	







## 3.2  Deuxième étape (3 séance)

### 3.2.1  Calcul d’une borne inférieure

+ Qu'est-ce qu'une borne inf?	
	La borne inférieur est un résultat très optimiste de l'évaluation de la fonction objectif. 	celle est `<=` aux solutions réalisable.
+ Proposer une méthode de calcul de la borne inf
	Pour le calcul de la borne inf nous avons choisi  de prendre la valeur maximale correspondante au temps de traversé des noeuds par un groupe seul sans tenir compte des capacités et flots maximals des arcs,  noeuds et groupes.

+ Application de notre méthode sur l'exemple fourni

```java
graphe-TD-sans-DL-data
3
1 47 0
2 0 0
3 0 0
valid
34
1
borne inf
...
```
+ Vérifiez à chaque fois si la solution produite est correcte (réalisable et valeur annoncée de l’objectifcorrecte).
	La solution produite n'est ni correcte ni réalisable

+ Pensez que vous pouvez écrire un script pour lancer tous ces calculs
	oui

	```java
    public static void calculBorneInf() {
        int inf = 0;
        for (Chemin ch : data.chemins) {
            int total = 0;
            int blockSize = (int) Math.ceil(ch.size / (double) ch.flow);
            for (int i = 0; i < ch.path.size() - 1; i++) {
                int start = ch.path.get(i);
                int end = ch.path.get(i + 1);
                Arc arc = data.getArc(start, end);
                total += arc.time;
            }
            total += blockSize;
            inf = (inf < total) ? total : inf;
        }
        System.out.println("Borne inferieure = " + inf);
    }
	```
+ Pouvez-vous donner sa complexité
	Soit N le nombre de chemins et M le nombre de noeuds traversés par un chemin:
	`L'algorithme a une complexité de O(N² x M)`




### 3.2.2  Calcul d’une borne supérieure


+ Proposition d'une heuristique de calcul de la borne supérieures
	
	L'heuristique que nous avons choisi consite a faire partir tous les groupes à la date 0 mais en faisant en sorte de ne jamais dépasser les capacités des arcs. Lorsque plusieurs groupes se croisent au niveau d'un noeud le flow que nous affectons à un groupe/chemin est le rapport de la capacité de ce noeud par le nomdre de groupes qui s'y croisent en supposant qu'ils y arrivent au même moment.

+ Code source de l'heuristique

```java
public static void generateSolution() {
	int temps = 0;
	int maxFlow = 0;
	for (Chemin ch : data.chemins) {
		temps = 0;
		ch.startDate = temps;
		ch.getMaxFlow(data);
		int blockSize = (int) Math.ceil(ch.size / (double) ch.maxFlow);
		int remainingBlock = ch.size % ch.maxFlow;
		// + flow => start
		// - flow - modulo => temps + blockSize - 1
		// - modulo => temps + blockSize
		for (int i = 0; i < ch.path.size() - 1; i++) {
			int start = ch.path.get(i);
			int end = ch.path.get(i + 1);
			Arc arc = data.getArc(start, end);
			if (arc == null) {
				System.err.println("Arcs null (non trouvé)");
				System.exit(-1);
			}
			arc.addEvent(temps, ch.maxFlow);
			if (remainingBlock == 0)
				arc.addEvent(temps + blockSize, -ch.maxFlow);
			else {
				arc.addEvent(temps + blockSize - 1, remainingBlock - ch.maxFlow);
				arc.addEvent(temps + blockSize, -remainingBlock);
			}

			if (i != ch.path.size() - 2)
				temps += arc.time;
		}
		temps += blockSize;
		updateBorneSup(temps);
	}
}
```
+ Appliquez votre heuristique sur l’exemple.

	Le résultat d'exécution de notre code est noté ci dessus

	```java
	graphe-TD-sans-DL-data
	3
	1 5 0
	2 3 0
	3 3 0
	valid
	38
	1.0
	Borne Sup
	...
	```

+ Appliquez votre heuristique sur les instances fournies. Vérifiez à chaque fois si la solution produiteest correcte (réalisable et valeur annoncée de l’objectif correcte). Aviez-vous pensé à écrire un scriptpour lancer ces tests ?
	Résultat de vérification de notre résultat par notre checkeur
  
  ```java
	is_valid: true
	capacity_ok: true
	deadline_ok: true
	end_date: true
	diff_to_best_valid: 0
	diff_to_best_capacity_only: 0
	diff_to_best_deadline_only: 0
	End date = [38]
	Compute Time: 2ms.
  ```

+ Pouvez-vous donner sa complexité ?
	Soit N le nombre de chemins et M le nombre de noeuds traversés par un chemin:
	`L'algorithme a une complexité de O(N² x M)`


## 3.3  Troisième étape (1,5 séance) : Recherche locale - Intensification

+ Définissez un ou plusieurs voisinage(s) permettant d’explorer l’espace de recherche. Pour ces voisi-nages, définissez également une ou plusieurs fonctions d’évaluation
  
	Les voisinages possibles de notre solution sont le résultat de soit la variation de 1 du flow d'un  chemin ou la variation de 1 de la date de départ d'un chemin de notre borne sup.

+ Réflechissez à la stratégie d’exploration des voisinages ainsi qu’aux conditions d’arrêt de la méthode de descente

	1. Stratégie d'exploration
		
		+ Pour l'exploration nous commencons par sélectionner de façon aléatoire un chemin parmis ceux présents au niveau de la borne sup;
		+ Ensuite nous incrémentons de 1 le flow de ce chemin
		+ Nous verifions ensuite grace à notre simulateur de la validité de cet voisin et du fait qu'il fournisse une meilleure solution que celle actuellement connue, 
  			```
			=> si oui nous sélectionnons ce voisin et continuons si non
			=> si non nous sélectionnons un nouveau chemin de facon aléatoire et recommencons le processus
			```
	2. Conditions d'arrêts
      	1. Nous avons sélectionné tous les chemins sans réussir à améliorer notre solution
      	2. Nous avons atteint la borne inf
      	3. Notre nombre limite maximun de tour de boucle fixé a été atteint

+ Implémentez la méthode de descente basée sur le(s) voisinages défini(s). Comment vous assurer quevotre méthode est bien une méthode de descente ?

```java
while (indicesFlow.size() > 0) {

	randomIndexFlow = ThreadLocalRandom.current().nextInt(0, indicesFlow.size());
	int tmp = indicesFlow.get(randomIndexFlow);
	Chemin chFlow = dataClone.chemins.get(tmp);
	//initialiser maxFlow avec chFlow.realMaxFlow
	maxFlow = dataClone.chemins.get(tmp).realMaxFlow;
	for (int flooow = 1; flooow < maxFlow; flooow++) {
		//On augmente son flow de 1
		dataClone.chemins.get(tmp).flow = dataClone.chemins.get(tmp).flow + flooow;
		dataClone.dropEvents();
		//On recalcule ses évènements
		simulate(dataClone, true);
		//On vérifie si la nouvelle solution trouvée est viable
		int checkerResult = checker(dataClone, dataClone.endDate);
		if (checkerResult >= 7 && dataClone.endDate <= data.endDate) {
			intensificationResult.fonctionObj = dataClone.endDate;
			intensificationResult.chemins.clear();
			intensificationResult.chemins.addAll(dataClone.chemins);
			System.out.println(">> Une nouvelle solution. old [" + data.endDate + "] => new [" + dataClone.endDate + "] maxDate = [" + maxDate + "]  value = [" + value + "] indiceDate.size() = [" + indicesDate.size() + "]");
			dataClone.printChemin(dataClone.chemins);
			data = new Data(dataClone);
			indicesDate.clear();
			indicesFlow.clear();
			for (int i = 0; i < data.chemins.size(); indicesDate.add(i), indicesFlow.add(i), i++) ;
			break loop;
		} else {
			chFlow.flow = chFlow.flow - flooow;
		}
	}
	indicesFlow.remove(randomIndexFlow);
}
```


## 3.4  Quatrième étape (1,5 séance) : Recherche locale - Diversification

Pour la diversification nous utilisons une méthode multistart modifiée

La Diversification contient en son cntre l'algorithme d'intensitfication. Elle se charge de le réxécuté jusqu'à ses propres conditions d'arrêts qui sont assez similaires à celles de l'intensification

1. Stratégie d'exploration

   + Pour l'exploration nous commencons par sélectionner de façon aléatoire un chemin parmis ceux présents au niveau de la borne sup;
   + Ensuite nous incrémentons de 1 la *date de départ* de ce chemin
   + Nous verifions ensuite grace à notre simulateur de la validité de cet voisin et du fait qu'il fournisse une meilleure solution que celle actuellement connue, 
	```
   	=> si oui nous sélectionnons ce voisin et continuons si non
   	=> si non nous sélectionnons un nouveau chemin de facon aléatoire et recommencons le processus
   	```
2. Conditions d'arrêts
    	1. Nous avons sélectionné tous les chemins sans réussir à améliorer notre solution
    	2. Nous avons atteint la borne inf
    	3. Notre nombre limite maximun de tour de boucle fixé a été atteint

3. Implémentez la méthode de recherche locale incluant les deux processus d’intensification et de diver-sification). Comment vous assurer que votre méthode comporte bien un processus de diversification ?

```java
static void intensificationEtDiversification() {
	Data dataClone = new Data(data);
	Vector<Integer> indicesDate = new Vector<>();
	Vector<Integer> indicesFlow = new Vector<>();
	for (int i = 0; i < dataClone.chemins.size(); indicesDate.add(i), indicesFlow.add(i), i++) ;
	int randomIndexDate = 0;
	int randomIndexFlow = 0;
	dataClone.endDate = 1;
	int maxDate = 5;
	int maxFlow = 5;
	int value = 0;
	int chances = 20;
	while (indicesDate.size() > 0 && chances >= 0) {
		//On selectionne un chemin de facon aleatoire
		chances --;
		randomIndexDate = ThreadLocalRandom.current().nextInt(0, indicesDate.size());
		Chemin chDate = dataClone.chemins.get(indicesDate.get(randomIndexDate));
		maxDate = ((data.endDate - chDate.tempsTraversee) > 0)? data.endDate - chDate.tempsTraversee : 4;
		System.out.println("chances [" + chances + "] - tempTraversée [" + chDate.tempsTraversee + "] => " + maxDate);
		loop:
		for (value = 1; value < maxDate; value++) {
			chDate.startDate = chDate.startDate + value;
			while (indicesFlow.size() > 0) {

				randomIndexFlow = ThreadLocalRandom.current().nextInt(0, indicesFlow.size());
				int tmp = indicesFlow.get(randomIndexFlow);
				Chemin chFlow = dataClone.chemins.get(tmp);
				//initialiser maxFlow avec chFlow.realMaxFlow
				maxFlow = dataClone.chemins.get(tmp).realMaxFlow;
				//Data tmpClone;
				for (int flooow = 1; flooow < maxFlow; flooow++) {
					//tmpClone = new Data(dataClone);
					//On augmente son flow de 1
					dataClone.chemins.get(tmp).flow = dataClone.chemins.get(tmp).flow + flooow;
					dataClone.dropEvents();
					//On recalcule ses évènements
					simulate(dataClone, true);
					//On vérifie si la nouvelle solution trouvée est viable
					int checkerResult = checker(dataClone, dataClone.endDate);
					if (checkerResult >= 7 && dataClone.endDate <= data.endDate) {
						intensificationResult.fonctionObj = dataClone.endDate;
						intensificationResult.chemins.clear();
						intensificationResult.chemins.addAll(dataClone.chemins);
						System.out.println(">> Une nouvelle solution. old [" + data.endDate + "] => new [" + dataClone.endDate + "] maxDate = [" + maxDate + "]  value = [" + value + "] indiceDate.size() = [" + indicesDate.size() + "]");
						dataClone.printChemin(dataClone.chemins);
						data = new Data(dataClone);
						indicesDate.clear();
//                            indicesDate.remove(randomIndexDate);
						indicesFlow.clear();
						for (int i = 0; i < data.chemins.size(); indicesDate.add(i), indicesFlow.add(i), i++) ;
						break loop;
					} else {
						chFlow.flow = chFlow.flow - flooow;
						//dataClone = tmpClone;
					}
				}
				indicesFlow.remove(randomIndexFlow);
			}
			chDate.startDate = chDate.startDate - value;

			indicesFlow.clear();
			for (int i = 0; i < data.chemins.size(); indicesFlow.add(i), i++) ;
		}

		if (value == maxDate){
			indicesDate.remove(randomIndexDate);
		}
		indicesFlow.clear();
		for (int i = 0; i < data.chemins.size(); indicesFlow.add(i), i++) ;
		System.out.println("[" + ((data.chemins.size() - indicesDate.size()) / (double)data.chemins.size() * 100) + "%]");
	}
}
```



## Conclusion

Les tableaux récapitulatifs des résultats d'exécution de notre algorithme sur les différentes instances se trouvent dans le dossiers tables à la racine de notre dépot git.