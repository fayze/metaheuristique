package tp1;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;


import java.lang.Math;

public class Tp1 {

    private static Data data = new Data();
    private static Result result = new Result();
    private static Result intensificationResult = new Result();
    private static ArrayList<Solution> exploredSolutions = new ArrayList<>();

    public static Integer int_(String str) {
        return Integer.parseInt(str);
    }

    // RIP int avec parametre, NEVER FORGET :'(
    public static int int_(String str, boolean up) {
        return (int) ((up) ? Math.ceil(Double.parseDouble(str)) : Math.floor(Double.parseDouble(str)));
    }

    public static void parseData(String line, AtomicInteger cmp) {

        String[] split = line.split(" ");
        if (split[0].equals("c")) {
            cmp.incrementAndGet();

        } else if (cmp.intValue() == 1) {
            if (split.length == 2) {
                data.numEvacNodes = Integer.parseInt(split[0]);
                data.idOfSafeNode = Integer.parseInt(split[1]);
            } else {
                Vector<Integer> values = new Vector<>();
                values.add(int_(split[0]));
                data.addArc(new Arc(int_(split[0]), int_(split[4])));
                for (int i = 4; i < split.length - 1; i++) {
                    values.add(int_(split[i]));
                    data.addArc(new Arc(int_(split[i]), int_(split[i + 1])));
                }
                values.add(int_(split[split.length - 1]));
                values.add(-1);
                data.addArc(new Arc(int_(split[split.length - 1]), -1, Integer.MAX_VALUE, 0, Integer.MAX_VALUE));
                data.chemins.add(new Chemin(int_(split[0]), int_(split[1]), int_(split[2]), values));
            }

        } else {
            if (split.length == 2) {
                data.numNode = Integer.parseInt(split[0]);
                data.numEdge = Integer.parseInt(split[1]);
            } else {
                int tmp = 0;
                try {
                    tmp = int_(split[2]);
                } catch (NumberFormatException e) {
                    tmp = Integer.MAX_VALUE;
                }

                int index = data.arcs.indexOf(new Arc(int_(split[0]), int_(split[1])));
                if (index != -1) {
                    data.arcs.get(index).set(tmp, int_(split[3]), int_(split[4]));
                }
            }
        }
    }

    public static void parseResult(String line, AtomicInteger cmp) {
        if (cmp.get() == 0)
            result.fileName = line;
        else if (cmp.get() == 1)
            result.nbrSommets = int_(line);
        else if (cmp.get() > 1 && cmp.get() < result.nbrSommets + 2) {
            String[] split = line.split(" ");
            int tmpFlow = int_(split[1]);
            result.sommets.add(new Sommet(int_(split[0]), tmpFlow, int_(split[2])));
            Chemin ch = data.getChemin(int_(split[0]));
            ch.startDate = int_(split[2]);
            if (ch.flow < tmpFlow) {
                System.err.println("invalid: (flow overflow node [" + ch.id + "])");
                System.exit(-1);
            }
            ch.flow = tmpFlow;
        } else if (cmp.get() == result.nbrSommets + 2)
            result.isValid = (line.equals("valid")) ? true : false;
        else if (cmp.get() == result.nbrSommets + 3)
            result.fonctionObj = (line.equals("inf")) ? Integer.MAX_VALUE : int_(line);
        else if (cmp.get() == result.nbrSommets + 4)
            result.tempsCalcul = Double.parseDouble(line);
        else if (cmp.get() == result.nbrSommets + 5)
            result.methode = line;
        else
            result.commentaire = line;
    }

    public static void readFiles(String fileName, boolean isInput) {
        AtomicInteger cmp = new AtomicInteger();
        try {
            Files.lines(Paths.get("./" + fileName)).forEach(line -> {
                if (!line.trim().isEmpty()) {
                    if (isInput)
                        parseData(line, cmp);
                    else {
                        parseResult(line, cmp);
                        cmp.incrementAndGet();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int computeEvents(Data donnees, Chemin ch, int temps, int blockSize, int remainingBlock) {
        // + flow => start
        // - flow - modulo => temps + blockSize - 1
        // - modulo => temps + blockSize
        ch.tempsTraversee = 0;
        for (int i = 0; i < ch.path.size() - 1; i++) {
            int start = ch.path.get(i);
            int end = ch.path.get(i + 1);
            Arc arc = donnees.getArc(start, end);
            if (arc == null) {
                System.err.println("Arcs null (non trouvé)");
                System.exit(-1);
            }

            arc.addEvent(temps, ch.flow);
            if (remainingBlock == 0)
                arc.addEvent(temps + blockSize, -ch.flow);
            else {
                arc.addEvent(temps + blockSize - 1, remainingBlock - ch.flow);
                arc.addEvent(temps + blockSize, -remainingBlock);
            }

            if (i != ch.path.size() - 2) {
                temps += arc.time;
                ch.tempsTraversee += arc.time;
            }
        }
        ch.tempsTraversee += blockSize + remainingBlock;
        return temps;
    }

    public static void simulate(Data donnees, boolean isForIntensification) {
        donnees.endDate = 0;
        Solution s = new Solution();
        for (Chemin ch : donnees.chemins) {
            int temps = ch.startDate;
            if(isForIntensification)
                s.add(ch.flow, ch.startDate);
            int blockSize = (int) Math.ceil(ch.size / (double) ch.flow);
            int remainingBlock = ch.size % ch.flow;
            temps = computeEvents(donnees, ch, temps, blockSize, remainingBlock);
            donnees.endDate = (donnees.endDate < temps + blockSize) ? temps + blockSize : donnees.endDate;
        }
        if(isForIntensification) {
//            if(exploredSolutions.contains(s)){
//                System.err.println("Solution dejà explorée");
//            }
            exploredSolutions.add(s);
        }
    }


    public static int checker(Data donnees, int fonctionObj) {
        /**
         * 0 => invalid
         * 3 => valid for duedate
         * 4 => valid for capacity
         * 7 => valid
         */

        int res = 0;
        boolean isValid = true, isCapacityValid = true, isDueDateValid = true, isFonctionObjValid = true;

        isFonctionObjValid = fonctionObj == donnees.endDate;

        for (Chemin ch : donnees.chemins) {
            if (ch.flow > ch.realMaxFlow) {
                isCapacityValid = false;
                isFonctionObjValid = false;
            }
        }

        for (Arc arc : donnees.arcs) {
            int max = arc.capacity;
            int currentFlow = 0;
            int dueDate = arc.dueDate;
            for (Pair p : arc.events) {
                currentFlow += p.value;
                if (currentFlow > max) {
                    //System.err.println("capacity error\n[" + arc.a + " => " + arc.b + "] overflow at " + p.time);
                    isCapacityValid = false;
                }
            }
            int realDueDate = arc.events.last().time + arc.time;
            if (realDueDate > dueDate) {
//                 System.err.println("duedate error\n[" + arc.a + " => " + arc.b + "] " + dueDate + " vs " + realDueDate);
                isDueDateValid = false;
                //System.exit(-1);
            }

        }


        //System.out.print("is_valid: ");

        res = (isDueDateValid)? res + 3 : res;
        res = (isCapacityValid) ? res + 4 : res;

        if (isCapacityValid && isDueDateValid && isFonctionObjValid) {
            //System.out.println("true");

        } else if (isCapacityValid && !isDueDateValid && isFonctionObjValid || !isCapacityValid && isDueDateValid && isFonctionObjValid) {
            //System.out.println("semi-valid");
        } else {
            //System.out.println("false");
        }
        // System.out.println("capacity_ok: " + isCapacityValid);
        //System.out.println("deadline_ok: " + isDueDateValid);
        // System.out.println("end_date: " + isFonctionObjValid);
        // System.out.println("diff_to_best_valid: " + 0);
        // System.out.println("diff_to_best_capacity_only: " + 0);
        // System.out.println("diff_to_best_deadline_only: " + 0);

        // System.out.println("\n\n\n\nEnd date = [" + donnees.endDate + "]");
        return res;
    }


    public static void calculBorneInf() {
        int inf = 0;
        for (Chemin ch : data.chemins) {
            int total = 0;
            int blockSize = (int) Math.ceil(ch.size / (double) ch.flow);
            for (int i = 0; i < ch.path.size() - 1; i++) {
                int start = ch.path.get(i);
                int end = ch.path.get(i + 1);
                Arc arc = data.getArc(start, end);
                total += arc.time;
            }
            total += blockSize;
            inf = (inf < total) ? total : inf;
        }
        System.out.println("Borne inferieure = " + inf);
    }

    public static void updateBorneSup(int dueDate) {
        result.borneSup = (result.borneSup < dueDate) ? dueDate : result.borneSup;
    }

    public static void generateSolution() {
        int temps = 0;
        int maxFlow = 0;
        for (Chemin ch : data.chemins) {
            temps = 0;
            ch.startDate = temps;
            ch.getMaxFlow(data);
            int blockSize = (int) Math.ceil(ch.size / (double) ch.maxFlow);
            int remainingBlock = ch.size % ch.maxFlow;
            // + flow => start
            // - flow - modulo => temps + blockSize - 1
            // - modulo => temps + blockSize
            for (int i = 0; i < ch.path.size() - 1; i++) {
                int start = ch.path.get(i);
                int end = ch.path.get(i + 1);
                Arc arc = data.getArc(start, end);
                if (arc == null) {
                    System.err.println("Arcs null (non trouvé)");
                    System.exit(-1);
                }
                arc.addEvent(temps, ch.maxFlow);
                if (remainingBlock == 0)
                    arc.addEvent(temps + blockSize, -ch.maxFlow);
                else {
                    arc.addEvent(temps + blockSize - 1, remainingBlock - ch.maxFlow);
                    arc.addEvent(temps + blockSize, -remainingBlock);
                }

                if (i != ch.path.size() - 2)
                    temps += arc.time;
            }
            temps += blockSize;
            updateBorneSup(temps);
        }
    }
    // 1
    // 2
    // ....
    // 7	flow 24
    // 8
    // 9

    static void intensificationEtDiversification() {
        Data dataClone = new Data(data);
        Vector<Integer> indicesDate = new Vector<>();
        Vector<Integer> indicesFlow = new Vector<>();
        for (int i = 0; i < dataClone.chemins.size(); indicesDate.add(i), indicesFlow.add(i), i++) ;
        int randomIndexDate = 0;
        int randomIndexFlow = 0;
        dataClone.endDate = 1; //why?
        int maxDate = 5;
        int maxFlow = 5;
        int value = 0;
        int chances = 20;
        while (indicesDate.size() > 0 && chances >= 0) {
            //On selectionne un chemin de facon aleatoire
            chances --;
            randomIndexDate = ThreadLocalRandom.current().nextInt(0, indicesDate.size());
            Chemin chDate = dataClone.chemins.get(indicesDate.get(randomIndexDate));
            maxDate = ((data.endDate - chDate.tempsTraversee) > 0)? data.endDate - chDate.tempsTraversee : 4;
//            maxDate = data.endDate - chDate.tempsTraversee;
//            if (maxDate <= 0){
//                System.out.println("continue");
//                indicesDate.remove(randomIndexDate);
//                continue;
//            }
            System.out.println("chances [" + chances + "] - tempTraversée [" + chDate.tempsTraversee + "] => " + maxDate);
            loop://remplacer maxDate par date (actuelle de fin - temps de traversé normal du chemin [somme des durées de traversée des arcs du chemin])
            for (value = 1; value < maxDate; value++) {
                chDate.startDate = chDate.startDate + value;
                while (indicesFlow.size() > 0) {

                    randomIndexFlow = ThreadLocalRandom.current().nextInt(0, indicesFlow.size());
                    int tmp = indicesFlow.get(randomIndexFlow);
                    Chemin chFlow = dataClone.chemins.get(tmp);
                    //initialiser maxFlow avec chFlow.realMaxFlow
                    maxFlow = dataClone.chemins.get(tmp).realMaxFlow;
                    //Data tmpClone;
                    for (int flooow = 1; flooow < maxFlow; flooow++) {
                        //tmpClone = new Data(dataClone);
                        //On augmente son flow de 1
                        dataClone.chemins.get(tmp).flow = dataClone.chemins.get(tmp).flow + flooow;
                        dataClone.dropEvents();
                        //On recalcule ses évènements
                        simulate(dataClone, true);
                        //System.out.println("Date [+" +value + "] " + indicesDate.get(randomIndexDate) + " Flow [+" +flooow + "] " + indicesFlow.get(randomIndexFlow));
                        //dataClone.printChemin(dataClone.chemins);

                        //On vérifie si la nouvelle solution trouvée est viable
                        int checkerResult = checker(dataClone, dataClone.endDate);
                        if (checkerResult >= 7 && dataClone.endDate <= data.endDate) {
                            intensificationResult.fonctionObj = dataClone.endDate;
                            intensificationResult.chemins.clear();
                            intensificationResult.chemins.addAll(dataClone.chemins);
                            System.out.println(">> Une nouvelle solution. old [" + data.endDate + "] => new [" + dataClone.endDate + "] maxDate = [" + maxDate + "]  value = [" + value + "] indiceDate.size() = [" + indicesDate.size() + "]");
                            dataClone.printChemin(dataClone.chemins);
                            data = new Data(dataClone);
                            indicesDate.clear();
//                            indicesDate.remove(randomIndexDate);
                            indicesFlow.clear();
                            for (int i = 0; i < data.chemins.size(); indicesDate.add(i), indicesFlow.add(i), i++) ;
                            break loop;
                        } else {
                            chFlow.flow = chFlow.flow - flooow;
                            //dataClone = tmpClone;
                        }
                    }
                    indicesFlow.remove(randomIndexFlow);
                }
                chDate.startDate = chDate.startDate - value;

                indicesFlow.clear();
                for (int i = 0; i < data.chemins.size(); indicesFlow.add(i), i++) ;
            }

            if (value == maxDate){
                indicesDate.remove(randomIndexDate);
            }
            //else
                System.out.println("fini");
            indicesFlow.clear();
            for (int i = 0; i < data.chemins.size(); indicesFlow.add(i), i++) ;
            //System.out.println("help");
            System.out.println("[" + ((data.chemins.size() - indicesDate.size()) / (double)data.chemins.size() * 100) + "%]");
        }
    }


    // TODO : Verifier que le flow maximale du premier noeud d'un chemin soit inferieur ou egal a la capacite du premier arc du chemin.
    // TODO : Recherche locale(?) voisinage(?)


    public static void main(String[] args) {
//         String fileName = "dense_10_30_3_10_I.full";
//          String fileName = "dense_10_30_3_8_I.full";
//        String fileName = "sparse_10_30_3_10_I.full";
        // String fileName = "sparse_10_30_3_8_I.full";
//        String fileName = "sparse_10_30_3_1_I.full";
       String fileName = "exemple.txt";
        String solutionName = "solution_" + fileName.split("/.")[0];
        Scanner sc = new Scanner(System.in);

        //File Loading and Parsing
        readFiles(fileName, true);

        long start = System.currentTimeMillis();
        generateSolution();
        long end = System.currentTimeMillis();
        System.out.println("Compute Time: " + (end - start) + "ms.");
        calculBorneInf();
        System.out.println("Borne superieure: " + result.borneSup);
        result.tempsCalcul = end - start;
        result.methode = "Bad Java Heuristic";
        result.commentaire = "...";
        result.generateFile(fileName, "bornesup_" + solutionName, data.chemins);
        System.out.println("\n------------------------------------\n");


        readFiles(solutionName, false);
        start = System.currentTimeMillis();
        data.dropEvents();
        simulate(data, false);
        checker(data, result.fonctionObj);
        System.out.println("Compute Time: " + (System.currentTimeMillis() - start) + "ms.");
        //data.print();
        // result.print();

        start = System.currentTimeMillis();
        intensificationEtDiversification();
        System.out.println("Compute Time: " + (System.currentTimeMillis() - start) + "ms.");

        intensificationResult.tempsCalcul = System.currentTimeMillis() - start;
        intensificationResult.methode = "intensification";
        intensificationResult.commentaire = "---";

        intensificationResult.generateFile(fileName, solutionName );
        sc.close();
    }

}
