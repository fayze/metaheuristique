package tp1;

import java.beans.VetoableChangeListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

//Class avec les données de solution
public class Result {
    public String fileName;
    public int nbrSommets;
    public Vector<Sommet> sommets = new Vector<>();
    public boolean isValid;
    public int borneInf;
    public int fonctionObj;
    public int borneSup;
    public double tempsCalcul;
    public String methode;
    public String commentaire;
    public Vector<Chemin> chemins = new Vector<>();

    public Result() {
    }

    public Result(String fileName, int nbrSommets, Vector<Sommet> sommets, boolean isValid, int fonctionObj,
            double tempsCalcul, String methode, String commentaire) {
        this.fileName = fileName;
        this.nbrSommets = nbrSommets;
        this.sommets = sommets;
        this.isValid = isValid;
        this.fonctionObj = fonctionObj;
        this.tempsCalcul = tempsCalcul;
        this.methode = methode;
        this.commentaire = commentaire;
    }

    public void generateFile(String inputFileName, String outputFileName, Vector<Chemin> chemins) {
        PrintWriter writer;
        String path = "./";
        try {
            writer = new PrintWriter(path + outputFileName, "UTF-8");
            writer.println(inputFileName);
            writer.println(chemins.size());
            chemins.forEach(ch -> writer.println(ch.id + " " + ch.maxFlow + " " + ch.startDate));
            writer.println("valid");
            writer.println(borneSup);
            writer.println(tempsCalcul);
            writer.println(methode);
            writer.print(commentaire);            
            writer.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void generateFile(String inputFileName, String outputFileName) {
        PrintWriter writer;
        String path = "./";
        try {
            writer = new PrintWriter(path + outputFileName, "UTF-8");
            writer.println(inputFileName.split("/.")[0]);
            writer.println(chemins.size());
            chemins.forEach(ch -> writer.println(ch.id + " " + ch.flow + " " + ch.startDate));
            writer.println("valid");
            writer.println(fonctionObj);
            writer.println(tempsCalcul);
            writer.println(methode);
            writer.print(commentaire);
            writer.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void print() {
        
         System.out.println("fileName: " + this.fileName);
         System.out.println("nbrSommets: " + this.nbrSommets);
         System.out.println("sommets");
         this.sommets.forEach(sommet-> sommet.print());
         System.out.println("isValid: " + this.isValid);
         System.out.println("fonctionObj: " + this.fonctionObj);
         System.out.println("tempsCalcul: " + this.tempsCalcul);
         System.out.println("methode: " + this.methode);
         System.out.println("commentaire: " + this.commentaire);
    }
    
}