package tp1;

import java.util.Vector;

//Class avec les données d'entrée
public class Data {
	public int numEvacNodes = 0, idOfSafeNode =0, numNode = 0, numEdge = 0;
	public Vector<Arc> arcs= new Vector<>();
	public Vector<Chemin> chemins = new Vector<>();
	public int endDate = 0;
	
	public Data(){}

	public Data(Data data){

		this.numEvacNodes = data.numEvacNodes;
		this.idOfSafeNode = data.idOfSafeNode;
		this.numNode = data.numNode;
		this.numEdge = data.numEdge;
		this.endDate = data.endDate;
		this.arcs.addAll(data.arcs);
		this.chemins.addAll(data.chemins);
	}

	public Data(int numEvacNodes, int idOfSafeNode, int numNode, int numEdge, Vector<Arc> arcs, Vector<Chemin> chemins,
			int endDate) {
		this.numEvacNodes = numEvacNodes;
		this.idOfSafeNode = idOfSafeNode;
		this.numNode = numNode;
		this.numEdge = numEdge;
		this.endDate = endDate;
		this.arcs.addAll(arcs);
		this.chemins.addAll(chemins);
	}

	public static void printChemin(Vector< Chemin > elements) {
		elements.forEach(element -> element.print());
		System.out.println();
	}
	
	public static void printArcs(Vector<Arc> elements) {
		elements.forEach(element -> {
			element.print();
		});
	}
	
	public void addArc(Arc a) {
		if(!arcs.contains(a))
			arcs.add(a);
		else {
			Arc b = getArc(a.a, a.b);
			b.compteur++;
		}
	}

	public Arc getArc(int a, int b) {
		int index = arcs.indexOf(new Arc(a,b));
		return (index == -1)? null : arcs.get(index);
	}
	
	public Chemin getChemin(int id) {
		for (Chemin ch : chemins) {
			if(ch.id == id)
			return ch;
		}
		return null;
	}

	public void dropEvents() {
		arcs.forEach(arc -> arc.events.clear());
	}

	public void print() {
		System.out.println("numEvacNodes = " + numEvacNodes + " idOfSafeNode = " + idOfSafeNode);
		System.out.println("\nChemins");
		printChemin(chemins);
		System.out.println("\nnumNode = " + numNode + " numEdge = " + numEdge);
		System.out.println("\nArcs");
		printArcs(arcs);
	}
}
