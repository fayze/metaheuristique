package tp1;

public class Pair {
    public int time = 0;
    public int value = 0;
	public Pair(int time, int value) {
		this.time = time;
		this.value = value;
	}

	public void merge(Pair other) {
		this.value += other.value;
	}

	public int getTime() {
		return time;
	}

	@Override
	public boolean equals(Object other) {
		if (other == this)
			return true;
		if (!(other instanceof Pair)) {
			return false;
		}
		return ((Pair) other).time == time;
	}
}