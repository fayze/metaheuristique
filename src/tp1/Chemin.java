package tp1;

import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

public class Chemin {
    public int id = -1;
    public int size = 0;
    public int flow = 0;
    public int maxFlow = 0;
    public int startDate = 0;
    public int realMaxFlow = 0;
    public Vector<Integer> path = new Vector<>();
    public int tempsTraversee = 0;

    public Chemin(int id){
        this.id = id;
    }

    public Chemin(int id, int size, int flow, Vector<Integer> path) {
        this.id = id;
        this.size = size;
		this.flow = flow;
        this.path = path;
        this.maxFlow = flow;
        this.realMaxFlow = flow;
    }


    public Chemin(Chemin ch){
        this.id = ch.id;
        this.size =ch.size;
        this.flow =ch.flow;
        this.maxFlow =ch.maxFlow;
        this.startDate =ch.startDate;
        this.realMaxFlow =ch.realMaxFlow;
        path.addAll(ch.path);
    }
    // public void getMaxFlow(Data data) {
    //     int flow =  this.flow;
    //     for (int i = 0; i < path.size()-1; i++) {
    //         int start = path.get(i);
    //         int end = path.get(i + 1);
    //         Arc arc = data.getArc(start, end);
    //         if (arc.capacity < flow)
    //             flow = arc.capacity;
    //     }
    //     this.maxFlow = flow;
    // }

    public void getMaxFlow(Data data) {
        int flow = this.flow;
        // Arc chosenArc = null;
        for (int i = 0; i < path.size() - 1; i++) {
            int start = path.get(i);
            int end = path.get(i + 1);
            Arc arc = data.getArc(start, end);
            arc.getModulo();
            int max = arc.getCapacity();
            // arc.setModuloMode(false);
            if (max < flow){
                //chosenArc = data.getArc(start, end);;
                flow = max;
            }
        }
        
        // if(chosenArc != null)
        //     chosenArc.setModuloMode(false);

        this.maxFlow = flow;
        //Il faut mettre à jour tous les arcs qu'on apporté une partie de son modulo, alors on parcours tous.
        for (int i = 0; i < path.size() - 1; i++) {
            int start = path.get(i);
            int end = path.get(i + 1);
            Arc arc = data.getArc(start, end);
            arc.setReminder(this.maxFlow);
        }
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;
        if (!(other instanceof Chemin)) {
            return false;
        }

        return ((Chemin) other).id == id ;
    }
  
    public void print() {
        System.out.print("[" + id + "] size = " + size + " flow = " + flow + " startDate = " + startDate + " | ");
        for (int i =0; i< path.size() - 1; i++) {
                System.out.print(path.get(i) + " => ");
        }
        System.out.println(path.lastElement());
    }

}