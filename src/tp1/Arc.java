package tp1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

//Class que define l'égalité des arcs dans un sens et a l'inverse
public class Arc{
	public int a = 0;
	public int b = 0;
	public int dueDate = 0;
	public int time = 0;
	public int capacity = 0;
	public int compteur = 1;
	public int modulo = 0;
	public int reminder = 0;
	public boolean moduloMode = true; //On n'utilise plus

	public TreeSet <Pair> events = new TreeSet<>((e1, e2) -> Integer.compare(e1.time, e2.time));
	//variable that check if there is  an overflow
	public int check = 0;
	
	public Arc(int a, int b) {
		this.a = a;
		this.b = b;
	}

	public Arc(int a, int b, int dueDate, int time, int capacity) {
		this(a, b);
		this.dueDate = dueDate;
		this.time = time;
		this.capacity = capacity;
	}

	public Arc(Arc arc){
		this.a = arc.a;
		this.b = arc.b;
		this.dueDate = arc.dueDate;
		this.time = arc.time;
		this.capacity = arc.capacity;
		this.compteur = arc.compteur;
		this.modulo = arc.modulo;
		this.reminder = arc.reminder;
		this.moduloMode = arc.moduloMode;
		this.check = arc.check;
		this.events.addAll(arc.events);
	}

	public void set(int dueDate, Integer time, Integer capacity){
		this.dueDate = dueDate;
		this.time = time;
		this.capacity = capacity;
	}

	public Pair findPair(Pair p) {
		this.check = 0;
		for (Pair event : events) {
			if(event.time <= p.time){
				this.check+= event.value;
				// System.out.println("515161651651651   " + check);
			}
			if(event.equals(p))
				return event;
		}
		return null;
	}

	public void addEvent(int eTime, int eValue) {
		Pair tmp = new Pair(eTime, eValue);
		Pair found = findPair(tmp);
		if(found != null)
			found.merge(tmp);
		else
			events.add(tmp);
		
		this.check+=eValue;
	}
	
	public int getModulo() {
		this.modulo = this.capacity % this.compteur;
		return this.modulo;
	}

	/**
	 * @return the capacity
	 */
	public int getCapacity() {
		int tmpCapacity = (int) Math.floor(this.capacity / (double) this.compteur);
		// return (this.moduloMode)? tmpCapacity + (this.modulo - this.reminder) : tmpCapacity;
		return tmpCapacity + (this.modulo - this.reminder); // (this.modulo - this.reminder) -> Le modulo disponible MOINS ce qu'on a déjà pris
	}

	public void setReminder(int delta) {
		int tmpCapacity = (int) Math.floor(this.capacity / (double) this.compteur);
		int tmp = delta - tmpCapacity;
		if(tmp > 0) //Si c'est positive c'est parce qu'on a pris partie du modulo.
			this.reminder += tmp;
		if(this.reminder > this.modulo) //On veut pas soustraire de plus au moment de faire getCapacity()
			this.reminder = this.modulo;
	}

	/**
	 * @param moduloMode the moduloMode to set
	 */
	public void setModuloMode(boolean moduloMode) {
		this.moduloMode = moduloMode;
	}

    @Override
	public boolean equals(Object other) {
        if (other == this) return true;
        if (!(other instanceof Arc)) {
            return false;
        }
		return ((Arc)other).a == a && ((Arc)other).b == b || ((Arc)other).b == a && ((Arc)other).a == b;
	}

	public void print() {
		System.out.println(this.a + " => " + this.b + " [");
		System.out.println("\t dueDate = " + this.dueDate + "\n\t length = "
				+ this.time + "\n\t capacity = " + this.capacity + "\n\t compteur = " + this.compteur 
				+ "\n\t heuristic MaxCapacity = " + (int) Math.floor(this.capacity / (double) this.compteur) + "\n\t heuristic modulo = " + this.modulo);

		System.out.println("\t Events [");
		// Collections.sort(events, (e1, e2) -> Integer.compare(e1.time, e2.time));
		System.out.print("\t ");
		events.forEach(event -> {
			System.out.print( " (" + event.time + ", " + event.value + ") ");
		});
		System.out.println("\n\t]");
		System.out.println("\n]");
	}
}
