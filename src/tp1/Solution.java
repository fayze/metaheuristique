package tp1;

import java.util.ArrayList;

public class Solution {
    public ArrayList<Integer> flows = new ArrayList<>();
    public ArrayList<Integer> startDates = new ArrayList<>();
    int sumsFlows = 0;
    int sumDates = 0;


    public Solution(){}

    public void add(int flow, int startDate){
        flows.add(flow);
        startDates.add(startDate);

        sumsFlows+= flow;
        sumDates+=startDate;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) return true;
        if (!(other instanceof Solution)) {
            return false;
        }
        Solution s = (Solution) other;
        if(s.sumDates != sumDates || s.sumsFlows!= sumsFlows)
            return false;
        if(!flows.equals(s.flows))
            return false;
        return startDates.equals(s.startDates);
    }
}
