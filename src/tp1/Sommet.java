package tp1;

//Element de solution
public class Sommet {
    public int id = 0;
    public int tauxEvac = 0;
    public int dateDebut = 0;
    

    public Sommet(int id, int tauxEvac, int dateDebut) {
		this.id = id;
        this.tauxEvac = tauxEvac;
        this.dateDebut = dateDebut;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;
        if (!(other instanceof Sommet)) 
            return false;
        return ((Sommet) other).id == id;
    }
    
    public void print(){
        System.out.println("id = " + this.id + "\ttaux = " + this.tauxEvac + "\tdate = " + this.dateDebut);
    }
}